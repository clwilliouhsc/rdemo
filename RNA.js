const fs = require('fs')

console.log("Starting Program...")

text = fs.readFileSync('./rosalind_rna.txt', 'utf8')
console.log("File Contents:", text)

result = ""

for (let i = 0; i < text.length; i++) {
    // console.log(text[i])
    if(text[i] == 'T') {result = result + 'U'}
    else {
        result = result + text[i]
    }
  }
console.log("Result:")
console.log(result)