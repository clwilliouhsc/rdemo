function AppViewModel() {

    var self = this;
    //have to make selected an observable so that UI is auto-updated
    self.mutations = ko.observableArray([
        {name:"Q52R", selected: ko.observable(false)}, 
        {name:"A67V", selected: ko.observable(false)},
        {name:"\u0394H69", selected: ko.observable(false)},
        {name:"\u0394V70", selected: ko.observable(false), group:1},
        {name:"V70F", selected: ko.observable(false), group:1},
        {name:"G75V", selected: ko.observable(false)},
        {name:"T76I", selected: ko.observable(false)},
        {name:"D80A", selected: ko.observable(false), group:2},
        {name:"D80G", selected: ko.observable(false), group:2},
        {name:"T95I", selected: ko.observable(false)},
        {name:"D138Y", selected: ko.observable(false)},
        {name:"\u0394G141", selected: ko.observable(false)},
        {name:"G142D", selected: ko.observable(false), group:3},
        {name:"\u0394G142", selected: ko.observable(false), group:3},
        {name:"\u0394G143", selected: ko.observable(false)},
        {name:"\u0394Y144", selected: ko.observable(false), group:4},
        {name:"Y144T", selected: ko.observable(false), group:4},
        {name:"Y145S", selected: ko.observable(false)},
        {name:"ins146N", selected: ko.observable(false)},
        {name:"W152C", selected: ko.observable(false)},
        {name:"E154K", selected: ko.observable(false)},
        {name:"\u0394E156", selected: ko.observable(false)},
        {name:"F157S", selected: ko.observable(false), group:5},
        {name:"\u0394F157", selected: ko.observable(false), group:5},
        {name:"R158G", selected: ko.observable(false)},
        {name:"N439K", selected: ko.observable(false)},
        {name:"L452R", selected: ko.observable(false), group:6},
        {name:"L452Q", selected: ko.observable(false), group:6},
        {name:"Y453F", selected: ko.observable(false)},
        {name:"S477R", selected: ko.observable(false), group:7},
        {name:"S477N", selected: ko.observable(false), group:7},
        {name:"T478K", selected: ko.observable(false)},
        {name:"E484K", selected: ko.observable(false), group:8},
        {name:"E484Q", selected: ko.observable(false), group:8},
        {name:"F490S", selected: ko.observable(false)},
        {name:"S494P", selected: ko.observable(false)},
        {name:"N501Y", selected: ko.observable(false)},
        {name:"H655Y", selected: ko.observable(false)},
        {name:"Q677H", selected: ko.observable(false)},
        {name:"P681H", selected: ko.observable(false), group:9},
        {name:"P681R", selected: ko.observable(false), group:9}
     ])

     self.variants = [
        {Location:'UK', Pango:'B.1.1.7', WHO:'Alpha', NEXT: '20I/501Y.V1', 
         muts:['\u0394H69', '\u0394V70', '\u0394Y144', 'E484K', 'S494P', 'N501Y', 'P681H'], score: 0},
    
        {Location:'South Africa', Pango:'B.1.351', WHO:'Beta', NEXT: '20H/501Y.V2',
         muts:['D80A', 'E484K', 'N501Y'], score: 0},
    
        {Location:' - ', Pango:'B.1.375', WHO:' - ', NEXT: '20C',
         muts:['\u0394H69', '\u0394V70', 'S477R'], score: 0},
    
        {Location:'US, California', Pango:'B.1.427', WHO:'Epsilon', NEXT: '20C/S:452R',
         muts:['L452R', 'Y453F',], score: 0},
     
        {Location:'US, California', Pango:'B.1.429', WHO:' - ', NEXT: '20C/S:452R',
         muts:['W152C', 'L452R', 'Y453F', 'Q677H',], score: 0},

        {Location:'UK, Nigeria', Pango:'B.1.525', WHO:'Eta', NEXT: '20A/S.484K',
         muts:['Q52R', 'A67V', '\u0394H69', '\u0394V70','\u0394Y144', 'N439K', 'Y453F', 'E484K', 'Q677H'], score: 0},

        {Location:'US, NY City', Pango:'B.1.526.1', WHO:'Iota', NEXT: '20C/S.484K',
         muts:['D80G', 'T95I', '\u0394Y144', 'F157S','L452R', 'S477N', 'E484K', ], score: 0},

        {Location:'India', Pango:'B.1.617.1', WHO:'Kappa', NEXT: '20A/S.154K; G/452.V3',
         muts:['T95I', 'G142D', 'E154K', 'L452R', 'E484Q', 'P681R', ], score: 0},

        {Location:'India', Pango:'B.1.617.2', WHO:'Delta', NEXT: '20A/S.478K',
         muts:['V70F', 'T95I', 'G142D', '\u0394E156', '\u0394F157', 'R158G', 'L452R', 'T478K', 'P681R', ], score: 0},

        {Location:'India', Pango:'B.1.617.3', WHO:'-', NEXT: '20A',
         muts:['G142D', 'L452R', 'E484Q', 'P681R', ], score: 0},

        {Location:'Cameroon', Pango:'B.1.620', WHO:'-', NEXT: '20A/S:126A',
         muts:['\u0394H69', '\u0394V70', '\u0394Y144', 'S477N', 'E484K', 'N501Y', 'P681H', ], score: 0},

        {Location:'Columbia', Pango:'B.1.621', WHO:'-', NEXT: '21H',
         muts:['T95I', 'Y144T', 'Y145S', 'ins146N', 'E484K', 'N501Y', 'P681H', ], score: 0},

        {Location:'Peru', Pango:'B.1.1.1.C37', WHO:'Lambda', NEXT: '21G',
         muts:['G75V', 'T76I', 'L452Q', 'F490S', ], score: 0},

        {Location:'Japan/Brazil', Pango:'P.1', WHO:'Gamma', NEXT: '20J/501Y.V3',
         muts:['D138Y', 'E484K', 'N501Y', 'H655Y', ], score: 0},

        {Location:'Brazil', Pango:'P.2', WHO:'Zeta', NEXT: '21J',
         muts:['E484K', ], score: 0},

         {Location:'Philippines', Pango:'P.3(B.1.1.28.3)', WHO:'Theta', NEXT: '-',
         muts:['\u0394G141', '\u0394G142', '\u0394G143','E484K', 'N501Y', 'P681H', ], score: 0}
         
        ]

     self.bestFit = ko.observableArray([]);

    //when a button is clicked, update its selected status
    self.toggleSelected = (mutation) => {
        mutation.selected(!mutation.selected())

        if(mutation.group) {
            self.mutations().forEach( item => {
                if (item.name != mutation.name && item.group == mutation.group)
                    item.selected(false)
            })
        }

        selections = self.getSelected()
        maxScore = 0;
        maxVariants = [];
        self.variants.forEach(variant => {
            tempScore = self.getScore(selections,variant)
            // console.log( variant.Location, tempScore)
            variant.score = tempScore
            if(tempScore > maxScore) {
                maxScore = tempScore
                maxVariants = [variant]
            } else if(tempScore == maxScore && maxScore > 0) {
                maxVariants.push(variant)
            }
        })

        console.log("Best fit:", maxVariants)
        self.bestFit(maxVariants)
    }

    self.getSelected = () => {
        result = []
        self.mutations().forEach( item => {
            if(item.selected()) {
                result.push(item.name)
            }
        })

        return result
    }

    self.getScore = (selections, variant) => {
        score = 0

        selections.forEach( item => {
            index = variant.muts.indexOf(item)
            if(index >= 0){
                score++
            }
        })

        return score
    }

    console.log("Knockout lodaed")

    

}


$(document).ready(function() {
    ko.applyBindings(new AppViewModel()); 
});