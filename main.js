const fs = require('fs')

console.log("Starting Program...")

text = fs.readFileSync('./rosalind_dna.txt', 'utf8')
console.log("File Contents:", text)

A = 0
C = 0
G = 0
T = 0

// console.log("string length:", text.length, text[1])
for (let i = 0; i < text.length; i++) {
    // console.log(text[i])
    if(text[i] == 'A') {A++}
    if(text[i] == 'C') {C++}
    if(text[i] == 'G') {G++}
    if(text[i] == 'T') {T++}
  }
console.log("Result:")
console.log(A,C,G,T)
